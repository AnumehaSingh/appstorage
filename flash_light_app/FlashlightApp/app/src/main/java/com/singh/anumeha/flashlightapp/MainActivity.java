package com.singh.anumeha.flashlightapp;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import java.security.Policy;

public class MainActivity extends AppCompatActivity {
    ImageButton imageButton;
    Camera camera;
    Parameters parameters;
    boolean isFlash = false;
    boolean isON = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageButton = (ImageButton)findViewById(R.id.imagebutton);

        if(getApplication().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH))
        {
            camera = Camera.open();
            parameters = camera.getParameters();
            isFlash = true;
        }

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isFlash){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Error....");
                    builder.setMessage("Flash light is not supported");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }

                if(isFlash) {
                    isFlashOn();
                }
                else{
                        isFlashOff();
                    }
                }
            });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(camera != null){
            camera.release();
            camera = null;
        }
    }

    public void isFlashOn(){
        if(!isON) {
            if(camera == null || parameters == null){
                return;
            }
            imageButton.setImageResource(R.drawable.on);
            parameters.setFlashMode(android.hardware.Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(parameters);
            camera.startPreview();
            isON = true;
        }

    }

    public void isFlashOff() {
        if (isON) {
            if(camera == null || parameters == null){
                return;
            }
            imageButton.setImageResource(R.drawable.off);
            parameters.setFlashMode(android.hardware.Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(parameters);
            camera.stopPreview();
            isON = false;
        }
}
}
